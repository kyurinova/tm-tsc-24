package ru.tsc.kyurinova.tm.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.exception.empty.EmptyNameException;
import ru.tsc.kyurinova.tm.exception.entity.TaskNotFoundException;
import ru.tsc.kyurinova.tm.model.Task;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public abstract class AbstractTaskCommand extends AbstractCommand {

    protected void showTask(@Nullable Task task) {
        @Nullable final String userId = serviceLocator.getAuthService().getUserId();
        @Nullable final List<Task> tasks = serviceLocator.getTaskService().findAll(userId);
        final Integer indexNum = tasks.indexOf(task) + 1;
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        System.out.println("Index: " + indexNum);
        System.out.println("Id: " + task.getId());
        System.out.println("Name: " + task.getName());
        System.out.println("Description: " + task.getDescription());
        System.out.println("Status: " + task.getStatus().getDisplayName());
        System.out.println("Project id: " + task.getProjectId());
    }

    @NotNull
    protected Task add(@Nullable final String name, @NotNull final String description) {
        if (!Optional.ofNullable(name).isPresent() || name.isEmpty()) throw new EmptyNameException();
        return new Task(name, description);
    }
}

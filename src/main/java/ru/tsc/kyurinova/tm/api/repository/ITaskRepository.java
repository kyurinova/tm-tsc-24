package ru.tsc.kyurinova.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.enumerated.Status;

import ru.tsc.kyurinova.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IOwnerRepository<Task> {

    @NotNull
    Task findByName(@NotNull String userId, @NotNull String name);

    @NotNull
    Task removeByName(@NotNull String userId, @NotNull String name);

    @NotNull
    Task startById(@NotNull String userId, @NotNull String id);

    @NotNull
    Task startByIndex(@NotNull String userId, @NotNull Integer index);

    @NotNull
    Task startByName(@NotNull String userId, @NotNull String name);

    @NotNull
    Task finishById(@NotNull String userId, @NotNull String id);

    @NotNull
    Task finishByIndex(@NotNull String userId, @NotNull Integer index);

    @NotNull
    Task finishByName(@NotNull String userId, @NotNull String name);

    @Nullable
    Task changeStatusById(@NotNull String userId, @NotNull String id, @NotNull Status status);

    @NotNull
    Task changeStatusByIndex(@NotNull String userId, @NotNull Integer index, @NotNull Status status);

    @NotNull
    Task changeStatusByName(@NotNull String userId, @NotNull String name, @NotNull Status status);

    @Nullable
    Task bindTaskToProjectById(@NotNull String userId, @NotNull String projectId, @NotNull String taskId);

    @Nullable
    Task unbindTaskToProjectById(@NotNull String userId, @NotNull String projectId, @NotNull String taskId);

    @Nullable
    Task findByProjectAndTaskId(@NotNull String userId, @NotNull String projectId, @NotNull String taskId);

    void removeAllTaskByProjectId(@NotNull String userId, @NotNull String projectId);

    @NotNull
    List<Task> findAllTaskByProjectId(@NotNull String userId, @NotNull String projectId);
}
